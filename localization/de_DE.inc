<?php

$labels = array();
$labels['userinfo'] = 'Konto';
$labels['title'] = 'Kontoinformationen';

$labels['account'] = 'Übersicht';
$labels['common'] = 'Allgemein';
$labels['username'] = 'Benutzername';
$labels['quota'] = 'Speicherplatz';
$labels['quota-total'] = 'Gesamt';
$labels['quota-used'] = 'In Verwendung';
$labels['quota-unlimited'] = 'unbegrenzt';

$labels['webmail'] = 'Webmail';
$labels['created'] = 'Erstellt';
$labels['last-login'] = 'Letzter Login';
$labels['default-identity'] = 'Identität';

$labels['mail'] = 'Mail-Konfiguration';
$labels['mail-incoming'] = 'Posteingangsserver';
$labels['mail-outgoing'] = 'Postausgangsserver';
$labels['mail-mx'] = 'DNS MX';
$labels['mail-mx-fallback'] = 'DNS MX (Backup)';
$labels['imap-prefix'] = 'IMAP-Pfad-Präfix';

$labels['abook'] = 'Adressbuch-Konfiguration';
$labels['ldap-version'] = 'LDAP-Version';
$labels['ldap-base'] = 'Suchbasis';
$labels['ldap-scope'] = 'Suchbereich';
$labels['ldap-scope-base'] = 'Basis';
$labels['ldap-scope-list'] = 'Ebene';
$labels['ldap-scope-sub'] = 'Subhierarchie';
$labels['ldap-filter'] = 'Filter';

$labels['list-join'] = ' oder ';
$labels['yes'] = 'Ja';
$labels['no'] = 'Nein';

$labels['type'] = 'Typ';
$labels['type-carddav'] = 'CardDAV';
$labels['type-imap'] = 'IMAPv4';
$labels['type-internal'] = 'Intern';
$labels['type-ldap'] = 'Verzeichnisdienst (LDAP)';
$labels['type-ldap2'] = 'Verzeichnisdienst (LDAPv2)';
$labels['type-ldap3'] = 'Verzeichnisdienst (LDAPv3)';
$labels['type-misc'] = 'Sonstige';
$labels['type-smtp'] = 'SMTP';
$labels['host'] = 'Servername';
$labels['ports'] = 'Port';
$labels['port-cleartext'] = '%1';
$labels['port-ssl'] = '%1 (TLS)';
$labels['port-tls'] = '%1 (STARTTLS)';
$labels['sasl-methods'] = 'Authentifizierungsmethoden';
$labels['sasl-method-*'] = 'Automatisch';
$labels['sasl-method-plain'] = 'Klartext';
$labels['sasl-method-login'] = 'Kennwort';
$labels['sasl-method-digest-md5'] = 'Digest Access (MD5)';
$labels['sasl-method-cram-md5'] = 'Challenge-Response (MD5)';
$labels['sasl-method-kerberos'] = 'Kerberos';
$labels['priority'] = 'Priorität';
$labels['writable'] = 'Schreibrechte';
$labels['groups'] = 'Unterstützt Gruppen';
$labels['capabilities'] = 'Erweiterungen';