<?php

$labels = array();
$labels['userinfo'] = 'Account';
$labels['title'] = 'Account informatie';

$labels['account'] = 'Overzicht';
$labels['common'] = 'Algemeen';
$labels['username'] = 'Gebruikersnaam';
$labels['quota'] = 'Opslag';
$labels['quota-total'] = 'Totaal';
$labels['quota-used'] = 'In gebruik';
$labels['quota-unlimited'] = 'onbeperkt';

$labels['webmail'] = 'Webmail';
$labels['created'] = 'Aangemaakt op';
$labels['last-login'] = 'Laatste login';
$labels['default-identity'] = 'Identiteit';

$labels['mail'] = 'Mail configuratie';
$labels['mail-incoming'] = 'Server inkomende e-mail';
$labels['mail-outgoing'] = 'Server uitgaande e-mail';
$labels['mail-mx'] = 'DNS MX';
$labels['mail-mx-fallback'] = 'DNS MX (backup)';
$labels['imap-prefix'] = 'Voorvoegsel IMAP-pad';

$labels['abook'] = 'Contacten configuratie';
$labels['ldap-version'] = 'LDAP versie';
$labels['ldap-base'] = 'Zoekbasis';
$labels['ldap-scope'] = 'Bereik';
$labels['ldap-scope-base'] = 'Basis';
$labels['ldap-scope-list'] = 'Eén niveau';
$labels['ldap-scope-sub'] = 'Subhiërarchie';
$labels['ldap-filter'] = 'Filter';

$labels['list-join'] = ' of ';
$labels['yes'] = 'Ja';
$labels['no'] = 'Nee';

$labels['type'] = 'Type';
$labels['type-carddav'] = 'CardDAV';
$labels['type-imap'] = 'IMAPv4';
$labels['type-internal'] = 'Intern';
$labels['type-ldap'] = 'Adreslijst (LDAP)';
$labels['type-ldap2'] = 'Adreslijst (LDAPv2)';
$labels['type-ldap3'] = 'Adreslijst (LDAPv3)';
$labels['type-misc'] = 'Overige';
$labels['type-smtp'] = 'SMTP';
$labels['host'] = 'Serveradres';
$labels['ports'] = 'Poort';
$labels['port-cleartext'] = '%1';
$labels['port-ssl'] = '%1 (TLS)';
$labels['port-tls'] = '%1 (STARTTLS)';
$labels['sasl-methods'] = 'Authenticatiemethodes';
$labels['sasl-method-*'] = 'Automatisch';
$labels['sasl-method-plain'] = 'Platte tekst';
$labels['sasl-method-login'] = 'Wachtwoord';
$labels['sasl-method-digest-md5'] = 'Digest Access (MD5)';
$labels['sasl-method-cram-md5'] = 'Challenge-Response (MD5)';
$labels['sasl-method-kerberos'] = 'Kerberos';
$labels['priority'] = 'Prioriteit';
$labels['writable'] = 'Schrijfrechten';
$labels['groups'] = 'Ondersteunt groepen';
$labels['capabilities'] = 'Uitbreidingen';