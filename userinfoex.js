/*
 * Extended User Information Roundcube plugin.
 * Copyright (C) 2014, Michael Maier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if (window.rcmail) {
	rcmail.addEventListener('init', function(evt) {
		var dn = 'userinfoex',
	    	tab = $('<span>').attr('id', 'settingstabpluginuserinfoex').addClass('tablink'),
	    	button = $('<a>').attr('href', rcmail.env.comm_path + '&_action=plugin.' + dn).html(rcmail.gettext('userinfo', dn)).appendTo(tab);

	    button.bind('click', function(e) {
			return rcmail.command('plugin.' + dn, this);
		});

	    // Add button and register command.
	    rcmail.add_element(tab, 'tabs');
	    rcmail.register_command('plugin.' + dn, function() {
			rcmail.goto_url('plugin.' + dn);
		}, true
		);
	});
}
