<?php
/*
 * Extended User Information Roundcube plugin.
 * Copyright (C) 2014, Michael Maier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adds a new settings entry providing the user with account information and configuration guides
 * for mail servers and LDAP address books.
 * Based on the original "userinfo" plugin shipped with Roundcube.
 *
 * @author Michael Maier <michael.maier@xplo-re.org>
 */
class userinfoex extends rcube_plugin
{
	const NS = 'userinfoex';

    public $task = 'settings';

    function init() {
		$this->load_config('config.inc.php.dist');
		$this->load_config('config.inc.php');
        $this->add_texts('localization/', true);
        $this->register_action('plugin.' . self::NS, array($this, 'onAction'));
        $this->include_script(self::NS . '.js');
		$this->include_stylesheet(self::NS . '.css');
    }

    function onAction() {
        $this->register_handler('plugin.body', array($this, 'onRender'));
        rcmail::get_instance()->output->send('plugin');
    }

    function onRender() {
		$rcube = rcube::get_instance();
        $rcmail = rcmail::get_instance();
		$rcmail->imap_init(true);
		$config = $rcmail->config;
        $user = $rcmail->user;
		$imap = $rcmail->imap;
		$username = $rcmail->get_user_name();

		$infoset = array();

		// Overview.
		if ($config->get('userinfoex_show_overview', true)) {
			$table =
				new html_table(array('cols' => 2, 'class' => 'propform'));

			$table->add(array('class' => 'title', 'width' => '120'), Q($this->gettext('username')));
			$table->add(null, Q($username));

			$infoset['account'] =
				html::tag('h3', array('class' => 'section-title'), Q($this->gettext('common'))) .
				$table->show();

			if ($config->get('userinfoex_show_overview_quota', true)) {
				$imap_quota = $imap->get_quota();

				if ($imap_quota) {
					$table =
						new html_table(array('cols' => 2, 'class' => 'propform'));

					if (!$imap_quota['total'] && $config->get('quota_zero_as_unlimited')) {
						$quota_total =
							$this->gettext('quota-unlimited');
					}
					else {
						$quota_total =
							show_bytes($imap_quota['total'] * 1024);
					}

					$table->add(array('class' => 'title', 'width' => '120'), Q($this->gettext('quota-total')));
					$table->add(null, Q($quota_total));

					$table->add(array('class' => 'title', 'width' => '120'), Q($this->gettext('quota-used')));
					$table->add(null, Q(show_bytes($imap_quota['used'] * 1024) . ' (' . $imap_quota['percent'] . '%)'));

					$infoset['account'] .=
						html::tag('h3', array('class' => 'section-title'), Q($this->gettext('quota'))) .
						$table->show();
				}
			}
		}

        // Webmail.
		if ($config->get('userinfoex_show_webmail', true)) {
	        $table =
	            new html_table(array('cols' => 2, 'class' => 'propform'));

	        $table->add(array('class' => 'title', 'width' => '120'), 'Web-ID');
	        $table->add(null, Q($user->ID));

	        $table->add('title', Q($this->gettext('username')));
	        $table->add(null, Q($user->data['username']));

	        $table->add('title', Q($this->gettext('created')));
	        $table->add(null, Q($rcmail->format_date($user->data['created'])));

	        $table->add('title', Q($this->gettext('last-login')));
	        $table->add(null, Q($rcmail->format_date($user->data['last_login'])));

	        $identity = $user->get_identity();
	        $table->add('title', Q($this->gettext('default-identity')));
	        $table->add(null, Q("{$identity['name']} <{$identity['email']}>"));

	        $infoset['webmail'] =
				$table->show();
		}

        // Mail configuration help.
		$enabled_ids =
			$config->get('userinfoex_show_mail', true);

		if ($enabled_ids) {
			$servers = array();

			// IMAP settings from user IMAP connection.
			if (!\is_array($enabled_ids) or \array_search('incoming', $enabled_ids) !== false) {
				if ($imap->options) {
					$servers['incoming'] = array(
						'type' => $imap->options['driver'],
						'host' => $imap->options['host'],
						'ports' => array(
							$imap->options['port'] => $imap->options['ssl'] ? 'ssl' : 'cleartext'
						),
						'sasl-methods' => array($imap->options['auth_type']),
						'username' => $imap->options['user'],
						'imap-prefix' => $imap->namespace['prefix'],
					);

					if ($config->get('userinfoex_show_mail_caps', true)) {
						$capability_keys = array(
							'acl', 'auth', 'binary', 'catenate', 'children', 'compress',
							'condstore', 'convert', 'enable', 'esearch', 'esort', 'filters',
							'i18nlevel', 'id', 'idle', 'imapsieve', 'language', 'list-status',
							'literal+', 'login-referr', 'mailbox-refe', 'metadata', 'move',
							'multiappend', 'multisearch', 'namespace', 'notify', 'qresync',
							'quota', 'sasl-ir', 'search=fuzzy', 'searchres', 'sort',
							'special-use', 'starttls', 'thread', 'uidplus', 'unselect',
							'url-partial', 'urlauth', 'utf8', 'within'
						);
						$capabilities = array();

						foreach ($capability_keys as $key) {
							$capability = $imap->get_capability($key);

							if ($capability) {
								$name_key = "imap-cap-$key";

								if ($rcube->text_exists($name_key, self::NS)) {
									$name =
										\sprintf('%s (%s)', \strtoupper($key), $this->gettext($name_key));
								}
								else {
									$name = \strtoupper($key);
								}

								if (\is_array($capability)) {
									foreach ($capability as &$property) {
										$property_key = "$name_key=$property";

										if ($rcube->text_exists($property_key, self::NS)) {
											$property =
												\sprintf('%s (%s)', $property, $this->gettext($property_key));
										}
									}

									$name .=
										': ' . \implode(', ', $capability);
								}

								$capabilities[$key] = $name;
							}
						}

						if ($capabilities) {
							$servers['incoming']['capabilities'] =
								\implode('; ', $capabilities);
						}
					}
				}
			}

			if (!\is_array($enabled_ids) or \array_search('outgoing', $enabled_ids) !== false) {
				// No SMTP configuration is accessible, hence we need to collect it ourselves.
				// Equals code in rcube_smtp.php.
		        $smtp_config = $rcmail->plugins->exec_hook('smtp_connect', array(
		            'smtp_server'    => $config->get('smtp_server'),
		            'smtp_port'      => $config->get('smtp_port', 25),
		            'smtp_user'      => $config->get('smtp_user'),
		            'smtp_pass'      => $config->get('smtp_pass'),
		            'smtp_auth_cid'  => $config->get('smtp_auth_cid'),
		            'smtp_auth_pw'   => $config->get('smtp_auth_pw'),
		            'smtp_auth_type' => $config->get('smtp_auth_type'),
		            'smtp_helo_host' => $config->get('smtp_helo_host'),
		            'smtp_timeout'   => $config->get('smtp_timeout'),
		            'smtp_conn_options'   => $config->get('smtp_conn_options'),
		            'smtp_auth_callbacks' => array(),
		        ));

				if ($smtp_config) {
					// Parse host configuration. Equals code in rcube_smtp.php.
			        $smtp_host = rcube_utils::parse_host($smtp_config['smtp_server']);

			        if (!$smtp_host) {
						$smtp_host = 'localhost';
					}

			        $smtp_port = \is_numeric($smtp_config['smtp_port']) ? $smtp_config['smtp_port'] : 25;
			        $smtp_host_url = \parse_url($smtp_host);

			        if (isset($smtp_host_url['host']) && isset($smtp_host_url['port'])) {
			            $smtp_host = $smtp_host_url['host'];
			            $smtp_port = $smtp_host_url['port'];
			        }

					// Simplified further processing for display.
					$smtp_port_type = 'cleartext';

					if (\substr($smtp_host, 0, 6) == 'ssl://') {
						$smtp_port_type = 'ssl';
						$smtp_host = \substr($smtp_host, 6);
					}
					elseif (\substr($smtp_host, 0, 6) == 'tls://') {
						$smtp_port_type = 'tls';
						$smtp_host = \substr($smtp_host, 6);
					}

					$servers['outgoing'] = array(
						'type' => 'smtp',
						'host' => $smtp_host,
						'ports' => array(
							$smtp_config['smtp_port'] => $smtp_port_type
						),
						'sasl-methods' => array($smtp_config['smtp_auth_type']),
						'username' => $smtp_config['smtp_user'],
					);
				}
			}

			$servers =
				$this->mergeSettings($enabled_ids, $servers, $config->get('userinfoex_mail', array()));

			$infoset['mail'] =
				$this->renderSet($servers, 'mail');
		}

        // Address book configuration help.
		$enabled_ids =
			$config->get('userinfoex_show_abooks', true);

		if ($enabled_ids) {
			$abooks = $rcmail->get_address_sources(false, true);
			$servers = array();
			$ldap_config = $config->get('ldap_public');

	        $username_fragments =
	            \explode('@', $username, 2);

	        $ldap_dc =
	            'dc=' . \strtr(@$username_fragments[1], array('.' => ',dc='));

			foreach ($abooks as $ident => $abook_base) {
				if (\is_array($enabled_ids) && false === \array_search($ident, $enabled_ids)) {
					continue;
				}

				$abook = $rcmail->get_address_book($ident);

				if (!$ident) {
					$adata = array(
						'type' => 'internal',
					);
				}
				elseif ($abook instanceof rcube_ldap) {
					$adata = array(
						'type' => 'ldap',
					);

					if (isset($ldap_config[$ident])) {
						$ldap_entry =
							$ldap_config[$ident];
						$ldap_ssl = false;
						$ldap_tls = false;

						// Include LDAP version in type (better readable than separate row).
						$adata['type'] .=
							$ldap_entry['ldap_version'];

						// Retrieve additional fields directly from LDAP configuration as their is
						// no public access to the internal parsed address book settings.
						if (\is_array($ldap_entry['hosts'])) {
							// Only one host supported for display, ignore remaining.
							// TODO: Should be improved to allow multiple host definitions.
							$ldap_host = $ldap_entry['hosts'][0];
						}
						else {
							$ldap_host = $ldap_entry['hosts'];
						}

						$ldap_host =
							rcube_utils::parse_host($ldap_host);

						if (\substr($ldap_host, 0, 6) === 'ssl://') {
							$ldap_ssl = true;
							$ldap_host = \substr($ldap_host, 6);
						}
						elseif (\substr($ldap_host, 0, 6) === 'tls://') {
							$ldap_tls = true;
							$ldap_host = \substr($ldap_host, 6);
						}

						$adata['host'] =
							$ldap_host;

						$ldap_tls =
							($ldap_tls or $ldap_entry['use_tls']);

						$adata['ports'] =
							array($ldap_entry['port'] => $ldap_ssl ? 'ssl' : ($ldap_tls ? 'tls' : 'cleartext'));

						$adata['ldap-base'] =
							\strtr(
								$ldap_entry['base_dn'],
								array(
									'%fu' => $username,
									'%dc' => $ldap_dc,
									'%u' => $username_fragments[0],
									'%d' => @$username_fragments[1],
								));

						$adata['ldap-scope'] = $ldap_entry['scope'];
						$adata['ldap-filter'] = $ldap_entry['filter'];
						$adata['sasl-methods'] = array($ldap_entry['auth_method']);

						$adata['username'] =
							\strtr(
								$ldap_entry['bind_dn'],
								array(
									'%fu' => $username,
									'%dc' => $ldap_dc,
									'%u' => $username_fragments[0],
									'%d' => @$username_fragments[1],
								));
					}
				}
				else {
					$adata = array(
						'type' => 'misc',
					);
				}

				// Double merge to keep order of existing $adata array but also prefer specific
				// values over the following generic ones.
				$adata = \array_merge(\array_merge($adata,
					array(
						'name' => $abook_base['name'],
						'writable' => !$abook_base['readonly'],
						'groups' => $abook_base['groups'],
					),
					$adata
					));

				$servers[$ident] =
					$adata;
			}

			$servers =
				$this->mergeSettings($enabled_ids, $servers, $config->get('userinfoex_abooks', array()));

			$infoset['abook'] =
				$this->renderSet($servers, 'abook');
		}

		// Render full option set.
		$render = '';

		foreach ($infoset as $key => $section) {
			$render .=
	            html::tag(
	                'fieldset',
	                array('class' => 'tab'),
	                html::tag(
	                    'legend',
	                    null,
	                    Q($this->gettext($key))
	                    ) .
	                $section
	                );
		}

        return
			html::div(
				null,
				html::tag(
					'h2',
					array('class' => 'boxtitle'),
					$this->gettext('title')
				) .
				html::tag(
					'form',
					array('class' => 'propform boxcontent tabbed'),
  				  	$render
                )
			);
    }

	protected function mergeSettings($enabled_ids, $set, $config_set) {
		// Each value is automatically overwritten with an explicit plugin configuration value,
		// if present, as those take precedence, as well as via configuration new entries can
		// be added.
		foreach ($set as $key => $value) {
			if (isset($config_set[$key])) {
				$set[$key] =
					\array_merge($value, $config_set[$key]);
			}
		}

		foreach ($config_set as $key => $value) {
			if (\is_array($enabled_ids) and false === \array_search($key, $enabled_ids)) {
				// Regardless of provided data, filter out disabled IDs.
				continue;
			}

			if (!isset($set[$key])) {
				$set[$key] = $value;
			}
		}

		// Username expansion.
		foreach ($set as &$entry) {
			if (isset($entry['username'])) {
				$entry['username'] =
					\str_replace('%u', rcmail::get_instance()->get_user_name(), $entry['username']);
			}
		}

		return $set;
	}

	protected function renderSet($set, $section) {
		$output = '';

		foreach ($set as $type => $server) {
			$table =
	            new html_table(array('cols' => 2, 'class' => 'propform'));

			foreach ($server as $key => $value) {
				switch ($key) {
				case 'sasl-methods':
					foreach ($value as &$method) {
						if (empty($method)) {
							$method = '*';
						}

						$method = $this->gettext("sasl-method-$method");
					}

					$value =
						\implode(', ', $value);
					break;

				case 'ports':
					foreach ($value as $port => &$portType) {
						$portType =
							\strtr($this->gettext("port-$portType"), array('%1' => $port));
					}

					$value =
						\implode($this->gettext('list-join'), $value);
					break;

				case 'ldap-scope':
				case 'type':
					$value = $this->gettext("$key-$value");
					break;

				case 'writable':
				case 'groups':
					$value = $value ? $this->gettext('yes') : $this->gettext('no');
					break;

				case 'name':
					continue 2;
				}

				if (\is_array($value)) {
					$value = \implode(', ', $value);
				}

		        $table->add(array('class' => 'title', 'width' => '120'), Q($this->gettext($key)));
		        $table->add('', Q($value));
			}

			if (isset($server['name'])) {
				$sectionTitle =
					$server['name'];
			}
			else {
				$sectionTitle =
					$this->gettext("$section-$type");
			}

	        $output .=
				html::tag('h3', array('class' => 'section-title'), Q($sectionTitle)) .
				$table->show();
		}

		return $output;
	}

}
